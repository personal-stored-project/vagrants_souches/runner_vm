#! /bin/bash
args=("$@")
tokens=("${args[@]:1}")

# Install Dependencies
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | bash
dnf install docker-ce docker-ce-cli containerd.io gitlab-runner docker-compose-plugin -y

# Start Docker
systemctl enable docker 
systemctl start docker
usermod -aG docker gitlab-runner

# Register Runners
for token in "${tokens[@]}"; do
    gitlab-runner register --url $1 --registration-token $token --executor docker --docker-image python:3.9.6-alpine --non-interactive
done

# Run Runners
nohup gitlab-runner run & sleep 1
